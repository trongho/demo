﻿
namespace Demo1
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textUsername = new System.Windows.Forms.TextBox();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.btLogin = new System.Windows.Forms.Button();
            this.bInsert = new System.Windows.Forms.Button();
            this.lvUser = new System.Windows.Forms.ListView();
            this.bDelete = new System.Windows.Forms.Button();
            this.bDeleteAll = new System.Windows.Forms.Button();
            this.chkValidate = new System.Windows.Forms.CheckBox();
            this.bUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textUsername
            // 
            this.textUsername.Location = new System.Drawing.Point(36, 23);
            this.textUsername.Name = "textUsername";
            this.textUsername.Size = new System.Drawing.Size(248, 20);
            this.textUsername.TabIndex = 0;
            this.textUsername.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(36, 59);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(248, 20);
            this.textPassword.TabIndex = 1;
            this.textPassword.TextChanged += new System.EventHandler(this.textPassword_TextChanged);
            // 
            // btLogin
            // 
            this.btLogin.AccessibleName = "";
            this.btLogin.Location = new System.Drawing.Point(36, 97);
            this.btLogin.Name = "btLogin";
            this.btLogin.Size = new System.Drawing.Size(81, 29);
            this.btLogin.TabIndex = 2;
            this.btLogin.Text = "Login";
            this.btLogin.UseVisualStyleBackColor = true;
            this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
            // 
            // bInsert
            // 
            this.bInsert.Location = new System.Drawing.Point(123, 97);
            this.bInsert.Name = "bInsert";
            this.bInsert.Size = new System.Drawing.Size(75, 29);
            this.bInsert.TabIndex = 3;
            this.bInsert.Text = "Insert";
            this.bInsert.UseVisualStyleBackColor = true;
            this.bInsert.Click += new System.EventHandler(this.bInsert_Click);
            // 
            // lvUser
            // 
            this.lvUser.HideSelection = false;
            this.lvUser.Location = new System.Drawing.Point(346, 23);
            this.lvUser.Name = "lvUser";
            this.lvUser.Size = new System.Drawing.Size(522, 242);
            this.lvUser.TabIndex = 5;
            this.lvUser.UseCompatibleStateImageBehavior = false;
            this.lvUser.SelectedIndexChanged += new System.EventHandler(this.lvUser_SelectedIndexChanged);
            // 
            // bDelete
            // 
            this.bDelete.Location = new System.Drawing.Point(36, 147);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(81, 30);
            this.bDelete.TabIndex = 6;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            // 
            // bDeleteAll
            // 
            this.bDeleteAll.Location = new System.Drawing.Point(123, 147);
            this.bDeleteAll.Name = "bDeleteAll";
            this.bDeleteAll.Size = new System.Drawing.Size(75, 30);
            this.bDeleteAll.TabIndex = 7;
            this.bDeleteAll.Text = "Delete All";
            this.bDeleteAll.UseVisualStyleBackColor = true;
            // 
            // chkValidate
            // 
            this.chkValidate.AutoSize = true;
            this.chkValidate.Location = new System.Drawing.Point(36, 195);
            this.chkValidate.Name = "chkValidate";
            this.chkValidate.Size = new System.Drawing.Size(64, 17);
            this.chkValidate.TabIndex = 8;
            this.chkValidate.Text = "Validate";
            this.chkValidate.UseVisualStyleBackColor = true;
            // 
            // bUpdate
            // 
            this.bUpdate.Location = new System.Drawing.Point(216, 97);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(68, 29);
            this.bUpdate.TabIndex = 9;
            this.bUpdate.Text = "Update";
            this.bUpdate.UseVisualStyleBackColor = true;
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 328);
            this.Controls.Add(this.bUpdate);
            this.Controls.Add(this.chkValidate);
            this.Controls.Add(this.bDeleteAll);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.lvUser);
            this.Controls.Add(this.bInsert);
            this.Controls.Add(this.btLogin);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.textUsername);
            this.Name = "LoginForm";
            this.Text = "LoginForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.Button btLogin;
        private System.Windows.Forms.Button bInsert;
        private System.Windows.Forms.ListView lvUser;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bDeleteAll;
        private System.Windows.Forms.CheckBox chkValidate;
        private System.Windows.Forms.Button bUpdate;
    }
}