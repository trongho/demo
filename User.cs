﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class User
    {
        public int id { set; get; }
        public String username { set; get; }
        public String password { set; get; }

        public Boolean isValidate { set; get; }

        public User(string username, string password,Boolean isValidate)
        {
            this.username = username;
            this.password = password;
            this.isValidate = isValidate;
        }

        public User()
        {
        }

    }
}
