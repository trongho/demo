﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo1
{
    class MainMethod
    {
        static public void Main(String[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            LoginForm();
        }

        public static void LoginForm()
        {
            Application.Run(new LoginForm());
        }

        
    }
}
