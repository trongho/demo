﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo1
{
    public partial class LoginForm : Form
    {
        public bool CheckBoxes { get; set; }
        public int id;
        public LoginForm()
        {
            InitializeComponent();
            setupListUser();
            showList();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            String username = textUsername.Text;
            String password = textPassword.Text;
            login(username, password);
            
        }

        private void textPassword_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void showList()
        {
            SqlConnection sqlConnection = DBUtils.getDBConnection();
            sqlConnection.Open();
            try
            {
                List<User> users = new List<User>();
                users = QueryUser(sqlConnection);
                lvUser.Items.AddRange(users.Select(p => new ListViewItem(new string[]
                {
                     p.username,
                     p.password,              
                     p.isValidate+"",
                     p.id+"",
                })).ToArray());


            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

        private void setupListUser()
        {
            lvUser.View = View.Details;
            lvUser.GridLines = true;
            lvUser.FullRowSelect = true;

            lvUser.Columns.Add("Username", 100);
            lvUser.Columns.Add("Password", 70);
            lvUser.Columns.Add("Is Validate", 70);
            lvUser.Columns.Add("ID",0);

            lvUser.CheckBoxes = true;
       
        }

        private void login(String username,String password){
            SqlConnection sqlConnection = DBUtils.getDBConnection();
            sqlConnection.Open();
            try
            {
                


            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }

        }

        private static List<User> QueryUser(SqlConnection sqlConnection)
        {
            List<User> users = new List<User>();
            SqlCommand sqlCommand;
            SqlDataReader sqlDataReader;
            String sql = "";
      
            sql = "Select [id],[username],[password],[isvalidate] from [User]";
            sqlCommand = new SqlCommand(sql, sqlConnection);
            sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                User user = new User();
                user.id = (int)sqlDataReader.GetValue(0);
                user.username = sqlDataReader.GetValue(1)+"";
                user.password = sqlDataReader.GetValue(2) + "";
                user.isValidate =(Boolean)sqlDataReader.GetValue(3);
                users.Add(user);
            }
            return users;
       
        }

        private static void InsertUser(SqlConnection sqlConnection)
        {
            SqlCommand sqlCommand;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            String sql = "";
            for(int i = 1; i < 10;i++)
            {
                sql = "Insert into [User] ([username],[password],[isvalidate]) values('user"+@i+"','123',0)";
                sqlCommand = new SqlCommand(sql, sqlConnection);
                sqlCommand.Parameters.AddWithValue("@i", i);
                sqlDataAdapter.InsertCommand = new SqlCommand(sql, sqlConnection);
                sqlDataAdapter.InsertCommand.ExecuteNonQuery();
                sqlCommand.Dispose();
            }
           
        }

        private static void UpdateUser(SqlConnection sqlConnection,int id,String username,String password)
        {
            SqlCommand sqlCommand;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            String sql = "";
            for (int i = 1; i < 10; i++)
            {
                sql = "Update [User] set [username]='@username',[password]='@password' where [id]='@id'";
                sqlCommand = new SqlCommand(sql, sqlConnection);
                sqlCommand.Parameters.AddWithValue("username", username);
                sqlCommand.Parameters.AddWithValue("password", password);
                sqlCommand.Parameters.AddWithValue("id", id);
                sqlDataAdapter.InsertCommand = new SqlCommand(sql, sqlConnection);
                sqlDataAdapter.InsertCommand.ExecuteNonQuery();
                sqlCommand.Dispose();
            }
        }

        public static void MainForm()
        {
            
        }

        private void bInsert_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = DBUtils.getDBConnection();
            sqlConnection.Open();
                InsertUser(sqlConnection);
                showList();
                sqlConnection.Close();
                sqlConnection.Dispose();
        }

        

        private void lvUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvUser.SelectedIndices.Count <= 0)
            {
                return;
            }
            int selectedIndex = lvUser.SelectedIndices[0];
            if (selectedIndex >= 0)
            {
                String username = lvUser.Items[selectedIndex].SubItems[0].Text;
                String password = lvUser.Items[selectedIndex].SubItems[1].Text;
                String isValidate =lvUser.Items[selectedIndex].SubItems[2].Text;
                id = int.Parse(lvUser.Items[selectedIndex].SubItems[3].Text);
       
                textUsername.Text = username;
                textPassword.Text = password;
                if (isValidate.Equals("False"))
                {
                    chkValidate.Checked = false;
                }
                else
                {
                    chkValidate.Checked = true;
                }
            }
        }

        private void bUpdate_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = DBUtils.getDBConnection();
            sqlConnection.Open();
            UpdateUser(sqlConnection, id, textUsername.Text, textPassword.Text);
            showList();
            sqlConnection.Close();
            sqlConnection.Dispose();
        }
    }
}
